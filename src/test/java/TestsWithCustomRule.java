import org.junit.ClassRule;
import org.junit.Test;

import com.ingesis.demo.MyCustomRule;


public class TestsWithCustomRule {

	@ClassRule
    public static MyCustomRule myRule = new MyCustomRule();

    @Test
    public void testMethod1() throws InterruptedException {
        System.out.println("running testMethod1()");
        Thread.sleep(200);
    }

    @Test
    public void testMethod2() throws InterruptedException {
        System.out.println("running testMethod2()");
        Thread.sleep(150);
    }

    @Test
    public void testMethod3() throws InterruptedException {
        System.out.println("running testMethod3()");
        Thread.sleep(100);
    }
}
