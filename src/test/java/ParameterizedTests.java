import org.assertj.core.api.Assertions;
import org.junit.Test;
import org.junit.runner.RunWith;

import junitparams.JUnitParamsRunner;
import junitparams.Parameters;

@RunWith(JUnitParamsRunner.class)
public class ParameterizedTests {

	//https://github.com/Pragmatists/JUnitParams

	@Test
	@Parameters({ "1", "3", "5", "-3", "15" })
	public void testOdd(int number) {
		Assertions.assertThat(Math.abs(number % 2)).isEqualTo(1);
	}
	
	@Test
	@Parameters({ "2, 4", "3, 9", "4, 16"}) 
	public void testPower(int number, int expected) {
	    Assertions.assertThat(Math.pow(number, 2)).isEqualTo(expected);
	}
	
}
