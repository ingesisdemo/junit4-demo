import org.assertj.core.api.Assertions;
import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Ignore;
import org.junit.Test;

public class MathTests {

	
	@BeforeClass
	public static void beforeAll() {
		System.out.println("Do things needed in all tests");
		System.setProperty("test.inactive", "true");
	}
	
	@Before
	public void beforeEachTest() {
		System.out.println("Create a new object before each test");
	}
	
	@After
	public void afterEachTest() {
		System.out.println("Clean up things created in Before");
	}
	
	
	@AfterClass
	public static void afterAll() {
		System.out.println("Clear things created in BeforeClass");
		System.clearProperty("test.inactive");
	}
	
	@Test
	public void testAdd() {
		int value = Math.addExact(4, 2);
		Assertions.assertThat(value).isEqualTo(6);
	}

	@Test
	@Ignore
	public void testToIgnore() {
		Assertions.fail("Should be ignored");
	}

	@Test
	public void testMin() {
		int value = Math.min(100, 10);
		Assertions.assertThat(value).isEqualTo(10);
	}
	
	@Test(expected = NumberFormatException.class)
	public void testThrowAnException() {
		Integer.parseInt("abcd");
	}

	
	@Test
	public void testExceptionAlternate() {
		try {
			Integer.parseInt("abcd");
			Assertions.fail("Should not reach this point");
		} catch(NumberFormatException e) {
			Assertions.assertThat(e.getMessage()).startsWith("For input string:");
		}
	}
	
	
	

}
