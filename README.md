**Examples using Junit4.**

The examples are in src/test/java

- MathTests 
    - Show the lifecycle
    - Basic Assertions with Assertj
    - Disable a test
    - Check that an exception have been thrown
    - Automatically fail if a code should have been executed

- ParameterizedTests
    - Show how to use [JUnitParams]( http://pragmatists.github.io/JUnitParams/)
         
- TestsWithCustomRule
    - Show how to apply a class rule.



**What makes a good unit test**
- Each test should run in less than 0.1 second
- The tests can be run in any order
- The tests do not create side effects, if you create a directory, delete it after the tests.
- Test only ingesis code, not 3rd party. MathTests is a bad example, no need to test java.util.Math
- Every test must run at any time in the future
- A Unit test that fail should be treated like a compilation error


Simple design change that can be done to improve testability: 


```
  public double calculatePrice() {
      LocalDate today = LocalDate.now();
      Random rand = new Random();
      int value = rand.nextInt(10);
      if(today.getMonth() == 4) {
        return 1000 + value;
      }
      return 1200 + value;
  }
```

  
  can be changed easily like:
  

  ```
// Do not test this method
  public double calculatePrice() {
      LocalDate today = LocalDate.now();
      Random rand = new Random();
      int value = rand.nextInt(10);
      return calculatePrice(today, value);
  }

  // This method can be tested
  public double calculatePrice(LocalDate refDate, int value) {
      if(today.getMonth() == 4) {
        return 1000 + value;
      }
      return 1200 + value;
  }
  
```

  